import React, { useState } from "react";
import Card from "../UI/Card";
import classes from "./AddUser.module.css";
import Button from "../UI/Button";
import ErrorModal from "../UI/ErrorModal";

export default function AddUser(props) {
  // Now manage the state by using useState and use array destructuring
  // starting state is an empty string
  const [enteredUsername, setEnteredUsername] = useState("");
  //  for maintaining age state
  const [enteredAge, setEnteredAge] = useState("");

  // State for the error message
  const [error, setError] = useState();

  // to maintain the data entered by the user in every key stroke
  const usernameChangeHandler = (event) => {
    // Now here we set the data what user entered
    setEnteredUsername(event.target.value);
  };

  // For age data handling and updating
  const ageChangeHandler = (event) => {
    // Now here we set the data what user entered
    setEnteredAge(event.target.value);
  };

  const errorhandler = () => {
    setError(null);
  };

  // Function to handle the user adding
  const addUserHandler = (event) => {
    // meaning that the default action that belongs to the event will not occur
    event.preventDefault();

    // Validation Cases
    if (enteredUsername.trim().length === 0 || enteredAge.trim().length === 0) {
      setError({
        title: "Invalid Input ",
        message: "Please enter name and age..! ",
      });
      return;
    }
    if (+enteredAge < 1) {
      setError({
        title: "Invalid age ",
        message: "Please enter age > 0..! ",
      });
      return;
    }
    console.log(enteredUsername, enteredAge);
    props.onAddUser(enteredUsername, enteredAge);
    setEnteredUsername("");
    setEnteredAge("");
  };

  return (
    <div>
      {error && (
        <ErrorModal
          title={error.title}
          message={error.message}
          onConfirm={errorhandler}
        />
      )}
      // A Card here is a custom component which include the form comp to
      display
      <Card className={classes.input}>
        <form onSubmit={addUserHandler}>
          <label htmlFor="username">User Name</label>
          <input
            id="username"
            type="text"
            value={enteredUsername}
            onChange={usernameChangeHandler}
          />

          <label htmlFor="age">Age (Years)</label>
          <input
            id="age"
            type="number"
            value={enteredAge}
            onChange={ageChangeHandler}
          />
          {/* here we are using the custom button component */}
          <Button type="submit">Add User</Button>
        </form>
        {/* To show the list of user */}
      </Card>
    </div>
  );
}

import React, { useState, useRef } from "react";
import Card from "../UI/Card";
import classes from "./AddUser.module.css";
import Button from "../UI/Button";
import ErrorModal from "../UI/ErrorModal";
import Wrapper from "../Helpers/Wrapper";

export default function AddUser(props) {
  const nameInputRef = useRef();
  const ageInputRef = useRef();

  // State for the error message
  const [error, setError] = useState();

  const errorhandler = () => {
    setError(null);
  };

  // Function to handle the user adding
  const addUserHandler = (event) => {
    // meaning that the default action that belongs to the event will not occur
    event.preventDefault();
    const enteredName = nameInputRef.current.value;
    const enteredUserAge = ageInputRef.current.value;

    // Validation Cases
    if (enteredName.trim().length === 0 || enteredUserAge.trim().length === 0) {
      setError({
        title: "Invalid Input ",
        message: "Please enter name and age..! ",
      });
      return;
    }
    if (+enteredUserAge < 1) {
      setError({
        title: "Invalid age ",
        message: "Please enter age > 0..! ",
      });
      return;
    }
    props.onAddUser(enteredName, enteredUserAge);
    nameInputRef.current.value = "";
    ageInputRef.current.value = "";
  };

  return (
    <Wrapper>
      {error && (
        <ErrorModal
          title={error.title}
          message={error.message}
          onConfirm={errorhandler}
        />
      )}
      {/* // A Card here is a custom component which include the form comp to
      display */}
      <Card className={classes.input}>
        <form onSubmit={addUserHandler}>
          <label htmlFor="username">User Name</label>
          <input id="username" type="text" ref={nameInputRef} />

          <label htmlFor="age">Age (Years)</label>
          <input id="age" type="number" ref={ageInputRef} />
          {/* here we are using the custom button component */}
          <Button type="submit">Add User</Button>
        </form>
        {/* To show the list of user */}
      </Card>
    </Wrapper>
  );
}

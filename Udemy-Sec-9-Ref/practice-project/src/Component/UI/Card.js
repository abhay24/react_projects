import React from "react";
import classes from "./Card.module.css";

export default function Card(props) {
  return (
    <div
      // mutiple css together on card component
      className={`${classes.card} ${props.className}`}
    >
      {/* to return the form from AddUser comp. we can use
          props.childern for showing component into the component tag
           */}
      {props.children}
    </div>
  );
}

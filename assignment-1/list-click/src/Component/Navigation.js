import React from 'react';

const Navigation=()=> {
  return (
    <div>
    <ul className="list-group" onClick={ (e) => {console.log(e.target)}}>
      <li className="list-group-item " aria-current="true" >An active item</li>
      <li className="list-group-item ">A second item</li>
      <li className="list-group-item">A third item</li>
      <li className="list-group-item">A fourth item</li>
      <li className="list-group-item">And a fifth one</li>
    </ul>
    </div>
  );
}
export default Navigation;
import React from "react";
import Items from "./Items";
import "./Groceries.css";

export default function Groceries(props) {
  return (
    <div className="setup">
      <h4 className="d-flex align-items-center ">
        <i className="fa fa-leaf cursor-pointer" aria-hidden="true"></i>
        <span>Groceries</span>
      </h4>
      <ul>
        {props.DUMMY_DATA.map((items) => {
          return (
            <Items
              items={items}
              key={items.id}
              addData={props.addData}
              type="Groceries"
            />
          );
        })}
      </ul>
    </div>
  );
}

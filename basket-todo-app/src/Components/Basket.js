import React from "react";
import "./Basket.css";
import Items from "./Items";

export default function Basket(props) {
  return (
    <div className="bucket">
      <div className="basketlist">
        <p>
          <i className="fa fa-shopping-basket" aria-hidden="true"></i>
          <span>Basket</span>
        </p>
        <p className="clearBasket">
          <i
            className="fa fa-trash"
            aria-hidden="true"
            onClick={props.deleteAllData}
          ></i>
        </p>
      </div>
      {props.bucket.length === 0 ? (
        <p>No Groceries</p>
      ) : (
        <ul>
          {props.bucket.map((items) => {
            return (
              <Items
                items={items}
                key={items.id}
                deleteData={props.deleteData}
                type="bucket"
              />
            );
          })}
        </ul>
      )}
    </div>
  );
}

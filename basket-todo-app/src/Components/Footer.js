import React from "react";
import "./Footer.module.css";

export default function Footer() {
  return (
    <footer>
      <div>
        <span>All</span>, <a href="#">Pending</a>, <a href="#">Purchased</a>
      </div>
    </footer>
  );
}

import React from "react";
import "./Items.css";

export default function Items(props) {
  const addDataHandler = () => {
    props.addData(props.items.id, props.items.title);
  };
  const deleteDataHandler = () => {
    props.deleteData(props.items.id);
  };
  return (
    <li>
      {props.type === "Groceries" ? (
        <i
          className="fa fa-solid fa-plus-square mx-3 green"
          onClick={addDataHandler}
        ></i>
      ) : (
        <i
          className="fa fa-solid fa-minus-square mx-3 red"
          onClick={deleteDataHandler}
        ></i>
      )}
      {props.items.title}
      {props.items.quantity && <span>{props.items.quantity}</span>}
    </li>
  );
}

import "./Header.css";

import React from "react";

export default function Header() {
  return (
    <div className="contains ">
      <i className="fa fa-shopping-basket fa-6" aria-hidden="true"></i>

      <h4 className="App-title">Hello, Basket!</h4>
    </div>
  );
}

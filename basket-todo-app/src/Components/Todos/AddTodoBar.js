import React, { useState } from "react";
import "../Todos/AddTodoBar.css";

export default function AddTodoBar({ data, searchHandler }) {
  const [searchItem, setSearchItem] = useState([]);

  const handleSearch = (event) => {
    const searchWord = event.target.value;
    if (searchWord.length > 3 || searchWord === "") {
      searchHandler(searchWord);

      // const newSearch = data.filter((value) => {
      //   return value.title.toLowerCase().includes(searchWord.toLowerCase());
      // });
      // if (searchWord === "") {

      //   setSearchItem([]);
      // } else {
      //   setSearchItem(newSearch);
      // }
    }
  };
  return (
    <nav>
      <form>
        <input
          type="text"
          placeholder="filter for e.g. Apple onChange (not implemented)"
          onChange={handleSearch}
        />
      </form>
      {console.log(searchItem.length)}
      {searchItem.length !== 0 && (
        <div className="dataResult">
          {searchItem.map((value, key) => {
            return <p className="dataItem">{value.title}</p>;
          })}
        </div>
      )}
    </nav>
  );
}

import React, { useState } from "react";
import "./App.css";
import Header from "./Components/Header";
import Footer from "./Components/Footer";
import AddTodoBar from "./Components/Todos/AddTodoBar";
import Groceries from "./Components/Groceries";
import Basket from "./Components/Basket";

export const DUMMY_DATA = [
  { id: "G0", title: "Strawberry" },
  { id: "G1", title: "Blueberry" },
  { id: "G2", title: "Orange" },
  { id: "G3", title: "Banana" },
  { id: "G4", title: "Apple" },
  { id: "G5", title: "Carrot" },
  { id: "G6", title: "Celery" },
  { id: "G7", title: "Mushroom" },
  { id: "G8", title: "Green Pepper" },
  { id: "G9", title: "Eggs" },
  { id: "G10", title: "Cheese" },
  { id: "G11", title: "Butter" },
  { id: "G12", title: "Chicken" },
  { id: "G13", title: "Beef" },
  { id: "G14", title: "Pork" },
  { id: "G15", title: "Fish" },
  { id: "G16", title: "Rice" },
  { id: "G17", title: "Pasta" },
  { id: "G18", title: "Bread" },
];

function App() {
  const [search, setSearch] = useState("");
  const [bucket, setBucket] = useState([]);
  const searchHandler = (data) => {
    setSearch(data);
  };
  console.log(search);
  const regex = new RegExp(search, "i");
  const filteredGroceriesList = DUMMY_DATA.filter((x) => {
    return regex.test(x.title);
  });
  const filteredBucketList = bucket.filter((x) => {
    return regex.test(x.title);
  });
  const addData = (id, title) => {
    const foundItem = bucket.find((item) => {
      return item.id === id;
    });
    console.log(foundItem);

    if (!foundItem) {
      setBucket((prevState) => {
        return [...prevState, { id, title, quantity: 1 }];
      });
    } else {
      const modifiedData = [...bucket];
      const index = modifiedData.findIndex((item) => {
        return item.id === id;
      });

      modifiedData[index].quantity++;
      setBucket(modifiedData);
    }
  };

  const deleteData = (id) => {
    const foundItem = bucket.find((item) => {
      return item.id === id;
    });
    // console.log(foundItem);

    if (foundItem.quantity > 1) {
      const modifiedData = [...bucket];
      const index = modifiedData.findIndex((item) => {
        return item.id === id;
      });

      modifiedData[index].quantity--;
      setBucket(modifiedData);
    } else if (foundItem.quantity === 1) {
      const newData = bucket.filter((item) => {
        return item.id !== id;
      });
      setBucket(newData);
    }
  };

  const deleteAllData = () => {
    setBucket([]);
  };

  return (
    <div className="App">
      {/* Header includes basket font and "Hello Basket" text */}
      <Header />

      {/* Add and Search bar component */}
      <AddTodoBar data={DUMMY_DATA} searchHandler={searchHandler} />
      <div className="wrapper">
        <Groceries
          DUMMY_DATA={filteredGroceriesList}
          addData={addData}
        ></Groceries>
        <Basket
          bucket={filteredBucketList}
          deleteData={deleteData}
          deleteAllData={deleteAllData}
        />
      </div>

      {/* Footer component */}
      <Footer />
    </div>
  );
}

export default App;

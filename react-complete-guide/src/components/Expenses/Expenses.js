import React, {useState} from 'react';
import './Expenses.css'
import Card from '../UI/Card';
import ExpensesFilter from './ExpensesFilter';
import ExpensesList from './ExpensesList';
import ExpensesChart from './ExpensesChart';

function Expenses(props){
    const [filteredYear, setFilteredYear] = useState('');
    const filterChangeHandler = selecterYear =>{
        setFilteredYear(selecterYear);
    }
    const filteredExpenses = props.expenses.filter(expense =>{
        return expense.date.getFullYear().toString() === filteredYear;
    })
  return(
    <div>        
    <Card className='expenses'>
    <ExpensesFilter selected={filteredYear} 
                    onChangeFilter={filterChangeHandler}/>

        <ExpensesChart expenses={filteredExpenses}/>
    {/* Call or Rendering the List of Data */}

        <ExpensesList expenses={filteredExpenses}/>
        
      </Card>
      </div>); 
}
export default Expenses;
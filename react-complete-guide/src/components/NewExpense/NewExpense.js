import React, {useState} from 'react'
import './NewExpense.css'
import ExpenseForm from './ExpenseForm';

function NewExpense(props) {

  const [isEditing, setIsEditing] =useState(false);
  const saveExpenseDataHandler =(enteredExpenseData)=>{
    // console.log(enteredExpenseData);
    const expenseData ={
      ...enteredExpenseData,
      id: Math.random().toString()
    }
    // console.log(expenseData);
    // console.log(expenseData);
    props.onAddExpense(expenseData);
    setIsEditing(false)
  }

  const startEditingHandler =()=>{ setIsEditing(true)}

  const stopEditinghandler =()=>{
    setIsEditing(false)
  }

  return (  
    <div className='new-expense'>
      {! isEditing && <button onClick={startEditingHandler}>Add New Expense</button>}
      {isEditing && <ExpenseForm 
          onSaveExpenseData={saveExpenseDataHandler}
          onCancel ={stopEditinghandler}
      />}
    </div>
  );
}
export default NewExpense;

import logo from './logo.svg';
import './App.css';
import UserRef from './UserRef';

function App() {
  return (
    <div className="App">
      <UserRef />
    </div>
  );
}

export default App;
